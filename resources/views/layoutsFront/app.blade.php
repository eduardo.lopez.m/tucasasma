<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>  
        <title>TUCASA SMA | Asesoría Profesional Inmobiliaria</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="generator" content="http://www.tucasasma.com">
        <meta name="author" content="TUCASA SMA">
        <meta name="contry" content="México">
        <meta name="state" content="San Miguel de Allende">
        <meta name="robots" content="index,follow">
        <meta name="googlebot" content="index,follow">
        <meta name="lenguage" content="Español">
        <meta name="revisit-after" content="2 days">
        <meta name="keywords" content="Inmobiliaria, Propiedades, Bienes Raíces, Renta, Venta, Casa, San Miguel de Allente, Real Estate, Asesoría Profesional, Mantenimiento, Administración, Guanajuato, México,">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <meta name="description" content="Asesoría Profesional, Mantenimiento y Administración, Inmobiliaria, Bienes Raíces, Renta y Venta de Propiedades">


        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900|Roboto+Mono:300,400,500"> 

        <link rel="stylesheet" href="{{ asset('publicFront/fonts/icomoon/style.css') }}">
        <link rel="stylesheet" href="{{ asset('publicFront/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('publicFront/css/magnific-popup.css') }}">
        <link rel="stylesheet" href="{{ asset('publicFront/css/jquery-ui.css') }}">
        <link rel="stylesheet" href="{{ asset('publicFront/css/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('publicFront/css/owl.theme.default.min.css') }}">
        <link rel="stylesheet" href="{{ asset('publicFront/css/bootstrap-datepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('publicFront/css/mediaelementplayer.css') }}">
        <link rel="stylesheet" href="{{ asset('publicFront/css/animate.css') }}">
        <link rel="stylesheet" href="{{ asset('publicFront/fonts/flaticon/font/flaticon.css') }}">
        <link rel="stylesheet" href="{{ asset('publicFront/css/fl-bigmug-line.css') }}">
        <link rel="stylesheet" href="{{ asset('publicFront/css/aos.css') }}">
        <link rel="stylesheet" href="{{ asset('publicFront/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('publicFront/css/hint.min.css') }}">

        <script src="{{ asset('publicFront/js/jquery-3.3.1.min.js') }}"></script>

        <!-- Sweet Alert -->
        <link href="{{ asset('css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">

        @php ini_set('max_execution_time', '300'); @endphp
        
    </head>

    <body>
        <a href="https://api.whatsapp.com/send?phone=524151533208" target="_blank"><img src="{{ asset('images/icono-whatsapp.png')}}" class="icono-right-contactar"></a>
        @include('layoutsFront.header')
        @yield('content')
        @include('layoutsFront.footer')
        @include('includesFront.js')
    </body>
</html>
