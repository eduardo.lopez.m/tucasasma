    <!-- Navbar Comercial-->
    <div class="site-wrap ">
      <div class="site-navbar site-navbarinfo">
        <div class="container py-1">
          <div class="row align-items-center">
            <div class="col-8 col-md-8 col-lg-8 text-left">
              <a href="tel:" id="idTelefonoHeaderAncla"><i class="icon-phone iconnavcontacto"></i> <span class="leyenda leyendasnavcontacto" id="idTelefonoHeader"></span></a>
              <a href="mailto:" id="idEmailHeaderAncla"><i class="icon-envelope iconnavcontacto"></i> <span class="leyenda leyendasnavcontacto leyendasnavcorreo" id="idEmailHeader"></span></a>
            </div>
            <div class="col-4 col-md-4 col-lg-4 text-right">
              <a href="#" target="_blank" id="idWhatsappHeader"><i class="icon-whatsapp iconnavrightredesultimo"></i><span class="leyenda"></span></a>
              <a href="#" target="_blank" id="idFacebookHeader"><i class="icon-facebook-square iconnavrightredes"></i><span class="leyenda"></span></a>
              <a href="#" target="_blank" id="idTwitterHeader"><i class="icon-youtube-square iconnavrightredes"></i><span class="leyenda"></span></a>
              <a href="#" target="_blank" id="idLinkedinHeader"><i class="icon-instagram iconnavrightredes"></i><span class="leyenda"></span></a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Navbar Menu -->
    <div class="site-wrap">
      <div class="site-mobile-menu">
        <div class="site-mobile-menu-header">
          <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
          </div>
        </div>
        <div class="site-mobile-menu-body"></div>
      </div> <!-- .site-mobile-menu -->

      <div class="site-navbar menu-sombreado mt-4-2">
        <div class="container py-1">
          <div class="row align-items-center">
            <div class="col-8 col-md-8 col-lg-4">
              <img src="{{ asset('images/logo_tucasasma2.svg')}}" width="100%" height="70px">
              <!-- <h1 class="mb-0"><a href="index.html" class="text-white h2 mb-0"><strong>Homeland<span class="text-danger">.</span></strong></a></h1> -->
            </div>
            <div class="col-4 col-md-4 col-lg-8">
              <nav class="site-navigation text-right text-md-right" role="navigation">

                <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle"><span class="icon-menu h3"></span></a></div>

                <ul class="site-menu js-clone-nav d-none d-lg-block">
                  <li id="mdlInicio"><a href="{{ asset('Inicio')}}">Inicio</a></li>
                  <li id="mdlPropiedades" class=""><a href="{{ asset('Propiedades')}}">Propiedades</a></li>
                  <li id="mdlAsesores" class=""><a href="{{ asset('Asesores')}}">Asesores</a></li>
                  <li id="mdlNosotros" class=""><a href="{{ asset('Nosotros')}}">Nosotros</a></li>
                  <li id="mdlContacto" class=""><a href="{{ asset('Contacto')}}">Contacto</a></li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>