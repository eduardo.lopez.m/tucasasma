@extends('layoutsFront.app')
@section('content')
  @foreach($propiedades as $propiedad)
    <div class="site-blocks-cover inner-page-cover overlay divPropiedades" style="background-image: url({{asset($propiedad->ln_url_imagen)}});" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">
          <div class="col-md-10">
            <h1 class="mb-2">{{$propiedad->ln_nombre}}</h1>
            <span class="d-inline-block text-white px-3 mb-3 property-offer-type rounded">{{$propiedad->ln_numero_casa}} {{$propiedad->ln_direccion}} </span>
            <p class="mb-5"><strong class="h2 text-success font-weight-bold">$ {{number_format($propiedad->nu_precio,2,'.',',')}} {{$propiedad->ln_moneda}} <strong class='lblTipoCambio hint--top' data-moneda="{{$propiedad->ln_moneda}}" data-nu_precio="{{$propiedad->nu_precio}}" aria-label=''><span class='icon-autorenew'></span></strong></strong></p>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section site-section-sm divPropiedades ">
      <div class="container ">
        <div class="row">
          <div class="col-lg-8">
            <div>
              <div class="slide-one-item home-slider owl-carousel">
                @foreach($imagenes as $imagen)
                  <div><img src="{{asset($imagen->ln_url_imagen)}}" alt="Image" class="img-fluid"></div>
                @endforeach
                
              </div>
            </div>
            <div class="bg-white property-body border-bottom border-left border-right">
              <div class="row mb-5">
                <div class="col-md-6">

                  <!-- @if($propiedad->ln_moneda=="USD") 
                    <strong class="text-success h1 mb-3">$ {{number_format($propiedad->nu_precio,2,'.',',')}} {{$propiedad->ln_moneda}} <div class='lblTipoCambio hint--top' data-moneda="{{$propiedad->ln_moneda}}" data-nu_precio="{{$propiedad->nu_precio}}" aria-label=''><span class=' icon-autorenew h3' ></span></div></strong>
                  @else
                    <strong class="text-success h1 mb-3">$ {{number_format($propiedad->nu_precio,2,'.',',')}} {{$propiedad->ln_moneda}}</strong>
                  @endif -->
                  <strong class="text-success h1 mb-3">$ {{number_format($propiedad->nu_precio,2,'.',',')}} {{$propiedad->ln_moneda}} <div class='lblTipoCambio hint--top' data-moneda="{{$propiedad->ln_moneda}}" data-nu_precio="{{$propiedad->nu_precio}}" aria-label=''><span class=' icon-autorenew h3' ></span></div></strong>
                  
                </div>
                <div class="col-md-6">
                  <ul class="property-specs-wrap mb-3 mb-lg-0  float-lg-right">
                    <li>
                      <span class="property-specs">Habitaciones</span>
                      <span class="property-specs-number">{{$propiedad->nu_dormitorio}}</span>
                      
                    </li>
                    <li>
                      <span class="property-specs">Baños</span>
                      <span class="property-specs-number">{{$propiedad->nu_banio}}</span>
                      
                    </li>
                    <li>
                      <span class="property-specs">Metros<sup>2</sup></span>
                      <span class="property-specs-number">{{$propiedad->nu_metros_terreno}}</span>
                      
                    </li>
                  </ul>
                </div>
                <div class="col-md-6 col-lg-8 text-center">
                  <span class="d-inline-block text-black mb-0 caption-text">Ubicación</span>
                  <strong class="d-block">
                    <a href="{{$propiedad->ln_url_mapa}}" target="_blank"><span class="property-icon icon-room"></span> {{$propiedad->ln_numero_casa}} {{$propiedad->ln_direccion}}</a>
                  </strong>
                </div>
                <?php $rutaCompartir = "https://".$_SERVER['HTTP_HOST']."/DetallesPropiedad/".$propiedad->nu_propiedad; ?>
                <div class="col-md-6 col-lg-4 text-center">
                  <span class="d-inline-block text-black mb-0 caption-text">Compartir</span>
                  <strong class="d-block">
                    <a style="font-size: 25px; margin-right: 10px;" href="https://api.whatsapp.com/send?text={{$rutaCompartir}}" target="_blank"><i class="icon-whatsapp"></i><span class="leyenda"></span></a>
                    <a style="font-size: 25px;" href="http://www.facebook.com/sharer.php?u={{$rutaCompartir}}&t={{$propiedad->ln_nombre}}" target="_blank"><i class="icon-facebook-square"></i><span class="leyenda"></span></a>
                  </strong>
                </div>
              </div>
              <div class="row mb-5">
                <div class="col-md-6 col-lg-4 text-center border-bottom border-top py-3">
                  <span class="d-inline-block text-black mb-0 caption-text">Tipo de Inmueble</span>
                  <strong class="d-block">{{$propiedad->ln_tipo_inmueble}}</strong>
                </div>
                <div class="col-md-6 col-lg-4 text-center border-bottom border-top py-3">
                  <span class="d-inline-block text-black mb-0 caption-text">Tipo Operación</span>
                  <strong class="d-block">{{$propiedad->ln_tipo_operacion}}</strong>
                </div>
                <div class="col-md-6 col-lg-4 text-center border-bottom border-top py-3">
                  <span class="d-inline-block text-black mb-0 caption-text">M<sup>2</sup> de vivienda</span>
                  <strong class="d-block">{{$propiedad->nu_metros_vivienda}}</strong>
                </div>
              </div>
              <h2 class="h4 text-black">Mas Información</h2>

              <!-- <p>30% de enganche y el resto a 6 meses.</p>
              <p>Hermosa residencia en Fraccionamiento Hacienda La Presita con vigilancia las 24 Hrs.</p>
              <p>Casa Club. 3 recamaras / Principal en PB, 3 1/2 baños, Sala, Comedor, desayunador, cocina, Lavandería ,Biblioteca ,Sala TV ,Terraza, Cochera para 2 autos</p> -->
              <p>{{$propiedad->ln_detalles}}</p>
            </div>
          </div>
          <div class="col-lg-4">

            <div class="bg-white widget border rounded">

              <h3 class="h4 text-black widget-title mb-3">Contactar Asesor</h3>
              <form action="" class="form-contact-agent" id="frmContacto">
                {{ csrf_field() }}
                <div class="form-group">
                  <label for="txtName">Nombre Completo <span id="txtNameError" style="color: red;" class="hide">Requerido</span></label>
                  <input type="text" id="txtName" name="txtName"  placeholder="Nombre Completo" class="form-control">
                </div>
                <div class="form-group">
                  <label for="txtEmail">Correo <span id="txtEmailError" style="color: red;" class="hide">Requerido</span></label>
                  <input type="email" id="txtEmail" name="txtEmail" placeholder="Correo"  class="form-control">
                </div>
                <div class="form-group">
                  <label for="txtTelefono">Teléfono <span id="txtTelefonoError" style="color: red;" class="hide">Requerido</span></label>
                  <input type="text" id="txtTelefono" name="txtTelefono" placeholder="Teléfono" class="form-control">
                </div>
                <div class="row form-group">
                  <div class="col-md-12">
                    <label class="font-weight-bold" for="txtAsunto">Asunto <span id="txtAsuntoError" style="color: red;" class="hide">Requerido</span></label>
                    <input type="text" id="txtAsunto" name="txtAsunto" class="form-control" placeholder="Agregar Asunto" value="Información {{$propiedad->ln_nombre}}">
                  </div>
                </div>
                <div class="row form-group" >
                  <div class="col-md-12">
                    <label class="font-weight-bold" for="txtMessage">Mensaje <span id="txtMessageError" style="color: red;" class="hide">Requerido</span></label> 
                    <textarea name="message" id="txtMessage" name="txtMessage" cols="30" rows="3" class="form-control" placeholder="Mensaje">{{$propiedad->ln_nombre}} {{$propiedad->ln_numero_casa}} {{$propiedad->ln_direccion}}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <input type="button" value="Enviar Mensaje" id="btnEnviarMensaje" class="btn btn-primary  py-2 px-4 rounded-0">
                </div>
              </form>
            </div>

            <div class="bg-white widget border rounded">
              <h3 class="h4 text-black widget-title mb-3">Galeria</h3>
              <div class="row no-gutters mt-4">
                @foreach($imagenes as $imagen)
                  <div class="col-sm-6 col-md-4 col-lg-4">
                    <a href="{{asset($imagen->ln_url_imagen)}}" class="image-popup gal-item"><img src="{{asset($imagen->ln_url_imagen)}}" alt="Image" class="img-fluid"></a>
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endforeach
@endsection