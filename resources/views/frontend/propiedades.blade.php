@extends('layoutsFront.app')
@section('content')

    <!-- Filtros & Orden -->
    <div class="site-section site-section-sm pb-0 bg-light ">
      <div class="container mt-6">
        <div class="row">
          <form class="form-search col-md-12">
            <div class="row  align-items-end">
              <div class="col-md-3">
                <label for="list-types">Tipo de Inmueble </label>
                <div class="select-wrap">
                  <span class="icon icon-arrow_drop_down"></span>
                  <select id ="cmbTipoInmueble" name="list-types" id="list-types" class="form-control d-block rounded-0">
                    <option value="">Seleccionar</option>
                    @foreach($combotipoinmueble as $inmueble)
                      @if($inmueble->nu_tipo_inmueble == $tipoinmueble)
                        <option value="{{$inmueble->nu_tipo_inmueble}}" selected>{{$inmueble->ln_tipo_inmueble}}</option>
                      @else
                        <option value="{{$inmueble->nu_tipo_inmueble}}">{{$inmueble->ln_tipo_inmueble}}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <label for="offer-types">Tipo de Oferta</label>
                <div class="select-wrap">
                  <span class="icon icon-arrow_drop_down"></span>
                  <select id ="cmbTipoOferta" name="offer-types" id="offer-types" class="form-control d-block rounded-0">
                    <option value="">Seleccionar</option>
                    @foreach($combotipooperacion as $oferta)
                      @if($oferta->nu_tipo_operacion== $tipooperacion)
                        <option value="{{$oferta->nu_tipo_operacion}}" selected>{{$oferta->ln_tipo_operacion}}</option>
                      @else
                        <option value="{{$oferta->nu_tipo_operacion}}">{{$oferta->ln_tipo_operacion}}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <label for="select-city">Ciudad</label>
                <div class="select-wrap">
                  <span class="icon icon-arrow_drop_down"></span>
                  <select name="select-city" id="select-city" class="form-control d-block rounded-0">
                    <option value="">San Miguel de Allende</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <label></label>
                <input type="button" class="linkFiltrarPropiedades btn btn-success text-white btn-block rounded-0 btn-lg " value="Buscar">
              </div>
            </div>
          </form>
        </div>  

        <div class="row">
          <div class="col-md-12 bg-white">
            <div class="view-options bg-white py-3 px-3 d-md-flex align-items-center">
              <div class="mr-auto">
                <a href="#" class="icon-view view-grid view-module active"><span class="icon-view_module"></span></a>
                <a href="#" class="icon-view view-list"><span class="icon-view_list"></span></a>
                
              </div>
              <div class="ml-auto d-flex align-items-center">
                <div>
                  <a style="cursor: pointer;" class="linkFiltrarPropiedaresReiniciar px-3 border-right active">Todo</a>
                  <a style="cursor: pointer;" class="linkFiltrarPropiedaresRenta px-3 border-right">En Renta</a>
                  <a style="cursor: pointer;" class="linkFiltrarPropiedaresVenta px-3">En Venta</a>
                </div>

                <div class="select-wrap" style="width: 150px;">
                  <span class="icon icon-arrow_drop_down"></span>
                  <select id="cmbOdernar" class="form-control form-control-sm d-block rounded-0">
                    <option value="">Ordenar por</option>
                    @if($ordenar=='1')
                      <option value="1" selected>Menor Precio</option>
                      <option value="2">Mayor Precio</option>
                    @elseif($ordenar=='2')
                      <option value="1">Menor Precio</option>
                      <option value="2" selected>Mayor Precio</option>
                    @else
                      <option value="1">Menor Precio</option>
                      <option value="2">Mayor Precio</option>
                    @endif
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
       
      </div>
    </div>

    <!-- Casas Vista Grid -->
    <div id="viewgrid" class="divPropiedades site-section site-section-sm bg-light show" >
      <div class="container">
        <div class="row mb-1" data-aos="fade-up" data-aos-delay="100">
          <!-- Seccion Casas -->
          @foreach($propiedades as $propiedad)
            <div class="col-md-6 col-lg-4 mb-4">
              <div class="property-entry h-100">
                <a href="/DetallesPropiedad/{{$propiedad->nu_propiedad}}" class="property-thumbnail">
                  <div class="offer-type-wrap">
                    @if($propiedad->nu_tipo_operacion == 1)
                      <span class="offer-type bg-success">{{$propiedad->ln_tipo_operacion}}</span>
                    @elseif($propiedad->nu_tipo_operacion == 2)
                      <span class="offer-type bg-info">{{$propiedad->ln_tipo_operacion}}</span>
                    @else
                      <span class="offer-type bg-danger">{{$propiedad->ln_tipo_operacion}}</span>
                    @endif
                  </div>
                  <img src="{{asset($propiedad->ln_url_imagen)}}" alt="Image" class="img-fluid">
                </a>
                <div class="p-4 property-body">
                  <h2 class="property-title"><a href="/DetallesPropiedad/{{$propiedad->nu_propiedad}}">{{$propiedad->ln_nombre}}</a></h2>
                  <span class="property-location d-block mb-3"><a href="{{$propiedad->ln_url_mapa}}" target="_blank"><span class="property-icon icon-room"></span> {{$propiedad->ln_numero_casa}} {{$propiedad->ln_direccion}}</a></span>
                  <!-- @if($propiedad->ln_moneda=="USD") 
                    <strong class="property-price text-primary mb-3 d-block text-success">$ {{number_format($propiedad->nu_precio,2,'.',',')}} {{$propiedad->ln_moneda}}  <div class='lblTipoCambio hint--top' data-moneda="{{$propiedad->ln_moneda}}" data-nu_precio="{{$propiedad->nu_precio}}" aria-label=''><span class='icon-autorenew' ></span></div></strong>
                  @else
                    <strong class="property-price text-primary mb-3 d-block text-success">$ {{number_format($propiedad->nu_precio,2,'.',',')}} {{$propiedad->ln_moneda}}</strong>
                  @endif -->
                  <strong class="property-price text-primary mb-3 d-block text-success">$ {{number_format($propiedad->nu_precio,2,'.',',')}} {{$propiedad->ln_moneda}}  <div class='lblTipoCambio hint--top' data-moneda="{{$propiedad->ln_moneda}}" data-nu_precio="{{$propiedad->nu_precio}}" aria-label=''><span class='icon-autorenew' ></span></div></strong>
                  <ul class="property-specs-wrap mb-3 mb-lg-0">
                    <li>
                      <span class="property-specs">Habitaciones</span>
                      <span class="property-specs-number">{{$propiedad->nu_dormitorio}}</span>
                    </li>
                    <li>
                      <span class="property-specs">Baños</span>
                      <span class="property-specs-number">{{$propiedad->nu_banio}}</span>
                    </li>
                    <li>
                      <span class="property-specs">Metros<sup>2</sup></span>
                      <span class="property-specs-number">{{number_format($propiedad->nu_metros_terreno)}}</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          @endforeach

        </div>
        
      </div>
    </div>

    <!-- Casas Vista Lista -->
    <div id="viewlist" class="divPropiedades site-section site-section-sm bg-light hide" >
      <div class="container">
        @foreach($propiedades as $propiedad)
          <div class="row mb-4">
            <div class="col-md-12">
              <div class="property-entry horizontal d-lg-flex">

                <a href="/DetallesPropiedad/{{$propiedad->nu_propiedad}}" class="property-thumbnail h-100">
                  <div class="offer-type-wrap">
                    @if($propiedad->nu_tipo_operacion == 1)
                      <span class="offer-type bg-success">{{$propiedad->ln_tipo_operacion}}</span>
                    @elseif($propiedad->nu_tipo_operacion == 2)
                      <span class="offer-type bg-info">{{$propiedad->ln_tipo_operacion}}</span>
                    @else
                      <span class="offer-type bg-danger">{{$propiedad->ln_tipo_operacion}}</span>
                    @endif
                  </div>
                  <img src="{{asset($propiedad->ln_url_imagen)}}" alt="Image" class="img-fluid">
                </a>

                <div class="p-4 property-body">
                  <h2 class="property-title"><a href="/DetallesPropiedad/{{$propiedad->nu_propiedad}}">{{$propiedad->ln_nombre}}</a></h2>
                  <span class="property-location d-block mb-3"><span class="property-icon icon-room"></span> {{$propiedad->ln_numero_casa}} {{$propiedad->ln_direccion}}</span>
                  
                  <!-- @if($propiedad->ln_moneda=="USD") 
                    <strong class="property-price text-primary mb-3 d-block text-success">$ {{number_format($propiedad->nu_precio,2,'.',',')}} {{$propiedad->ln_moneda}}  <div class='lblTipoCambio hint--top' data-nu_precio="{{$propiedad->nu_precio}}" aria-label=''><span class='icon-autorenew' ></span></div></strong>
                  @else
                    <strong class="property-price text-primary mb-3 d-block text-success">$ {{number_format($propiedad->nu_precio,2,'.',',')}} {{$propiedad->ln_moneda}}</strong>
                  @endif -->

                  <strong class="property-price text-primary mb-3 d-block text-success">$ {{number_format($propiedad->nu_precio,2,'.',',')}} {{$propiedad->ln_moneda}}  <div class='lblTipoCambio hint--top' data-moneda="{{$propiedad->ln_moneda}}" data-nu_precio="{{$propiedad->nu_precio}}" aria-label=''><span class='icon-autorenew' ></span></div></strong>

                  <p>{{$propiedad->ln_detalles}}</p>
                  <ul class="property-specs-wrap mb-3 mb-lg-0">
                    <li>
                      <span class="property-specs">Habitaciones</span>
                      <span class="property-specs-number">{{$propiedad->nu_dormitorio}}</span>
                    </li>
                    <li>
                      <span class="property-specs">Baños</span>
                      <span class="property-specs-number">{{$propiedad->nu_banio}}</span>
                    </li>
                    <li>
                      <span class="property-specs">Metros<sup>2</sup></span>
                      <span class="property-specs-number">{{number_format($propiedad->nu_metros_terreno)}}</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        @endforeach      
      </div>
    </div>

    <script src="{{ asset('publicFront/ajax/propiedades.js') }}"></script>

@endsection