@extends('layouts.app')
@section('content')
    
    <!-- Apartado del Breadcrumb -->
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <br>
            <ol class="breadcrumb">
                <li>
                    <a href="">Inicio</a>
                </li>
                <li>
                    <a href="">Propiedades</a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <!-- campos de criterios de busqueda -->
    <div class="wrapper wrapper-content animated fadeInRight" id="divFiltros">
        <div class="row">
            <!-- FILTROS -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Criterios de Búsqueda</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form role="form" id="frmFiltros">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nombre Casa</label> 
                                        <input name="txtNombre"  type="text" id="txtNombre" placeholder="Nombre Casa" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Moneda</label> 
                                        <select name='cmbMoneda' id='cmbMoneda' class="form-control selectMoneda"></select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="ibox-footer" style="text-align: center;">
                        <button id = "btnBuscar" class="btn btn-success btn-sm" type="button"><i class="fa fa-search"></i>&nbsp;&nbsp;<span class="bold">Buscar</span></button>
                        <button id ="btnNuevo" class="btn btn-success btn-sm" type="button"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span class="bold">Nuevo</span></button>
                    </div>
                </div>
            </div>

            <!-- TABLA DE BUSQUEDA -->
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Propiedades</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table id="tblRegistros" class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                    <th>Nombre</th>
                                        <th>Dirección</th>
                                        <th>Precio</th>
                                        <th>Moneda</th>
                                        <th>Carrusel</th>
                                        <th>Activo</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight" id="divFormulario" style="display: none;">
        <div class="row">
            
            <div class="col-lg-12" >
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="lblFormulario">Propiedad</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form role="form" id="frmFormulario" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="ln_nombreError">
                                        <label>* Nombre Casa</label> 
                                        <input name="nu_propiedad"  type="hidden" id="nu_propiedad" class="form-control" readonly>
                                        <input name="ln_nombre"  type="text" id="ln_nombre" placeholder="Nombre Casa" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group" id="nu_precioError">
                                        <label>* Precio</label>
                                        <input name="nu_precio"  type="text" id="nu_precio" placeholder="Precio" class="form-control"> 
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group" id='ln_monedaError'>
                                        <label>* Moneda</label> 
                                        <select name='ln_moneda' id='ln_moneda' class="form-control selectMoneda selectFormatoGeneral"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Número Inmueble</label> 
                                        <input name="ln_numero_casa"  type="text" id="ln_numero_casa" placeholder="Número Inmueble" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Habitaciones</label> 
                                        <input name="nu_dormitorio"  type="text" id="nu_dormitorio" placeholder="Número Habitaciones" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Baños</label> 
                                        <input name="nu_banio"  type="text" id="nu_banio" placeholder="Número Baños" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Cocheras</label> 
                                        <input name="nu_cochera"  type="text" id="nu_cochera" placeholder="Número Cocheras" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group" id='nu_tipo_inmuebleError'>
                                        <label>* Tipo Inmueble</label> 
                                        <select name='nu_tipo_inmueble' id='nu_tipo_inmueble' class="form-control selectTipoInmueble selectFormatoGeneral"></select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group" id='nu_tipo_operacionError'>
                                        <label>* Tipo Operación</label> 
                                        <select name='nu_tipo_operacion' id='nu_tipo_operacion' class="form-control selectTipoOperacion selectFormatoGeneral"></select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>M<sup>3</sup> Terreno</label> 
                                        <input name="nu_metros_terreno"  type="text" id="nu_metros_terreno" placeholder="Metros Terreno" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>M<sup>3</sup> Vivienda</label> 
                                        <input name="nu_metros_vivienda"  type="text" id="nu_metros_vivienda" placeholder="Metros Vivienda" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Dirección</label> 
                                        <input name="ln_direccion"  type="text" id="ln_direccion" placeholder="Calle, Municipio, Estado" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Correo</label> 
                                        <input name="ln_correo"  type="text" id="ln_correo" placeholder="Correo" value="tucasasma@gmail.com" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Télefono</label> 
                                        <input name="ln_telefono"  type="text" id="ln_telefono" placeholder="Ejemplo: +52 (415) 153 32 08" value="+52 415 153 32 08" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nu_activo">Activo</label><br>
                                        <input type="checkbox" id="nu_activo" name="nu_activo" class="i-checks" style="position: absolute; opacity: 0;" checked>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nu_activo">Carrusel</label><br>
                                        <input type="checkbox" id="nu_principal" name="nu_principal" class="i-checks" style="position: absolute; opacity: 0;" checked>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Detalles</label> 
                                        <textarea id="ln_detalles" name ="ln_detalles" class="form-control" rows="2" style="resize: vertical;"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Mapa</label> 
                                        <textarea id="ln_url_mapa" name ="ln_url_mapa" class="form-control" rows="1" style="resize: vertical;" placeholder="https://goo.gl/maps/A7iQNABTydhmWf2R8"></textarea>
                                    </div>
                                    <strong>Nota:</strong>
                                    <p>Ir a <a target="_blank" href="https://goo.gl/maps/hZK4yk6RAzAKsMN37">Google Maps</a> buscar el sitio, posteriormente click en la opción compartir y copiar enlace.</p>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group" >
                                        <label>* Subir Imagenes</label> 
                                        <input type="file" id="fileImagenes" name="fileImagenes[]" accept="image/png, image/jpeg" multiple>
                                        <p class="help-block">png, jpg.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divImagenes">
                                
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>Nota</strong>
                                    <p>Carrusel: Se recomienda tener 3 a 4 Casas selesccionas para el carrusel para evitar que tarde al cargar la pagina.</p>
                                    <p>Imagenes: Se recomienda que la imagen principal tenga las dimensiones de 800 x 450</p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="ibox-footer" style="text-align: center;">
                        <button id = "btnRegresar" class="btn btn-primary btn-sm" type="button"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;<span class="bold">Regresar</span></button>
                        <button style="display:none;" id = "btnAgregarNuevo" class="btn btn-primary btn-sm" type="button"><i class="fa fa-plus"></i>&nbsp;&nbsp;<span class="bold">Agregar</span></button>
                        <button style="display:none;" id = "btnModificarRegistro" class="btn btn-primary btn-sm" type="button"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;<span class="bold">Guardar</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('ajax/adminpropiedades.js') }}"></script>

@endsection
