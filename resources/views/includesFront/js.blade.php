<script src="{{ asset('publicFront/js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('publicFront/js/jquery-ui.js') }}"></script>
<script src="{{ asset('publicFront/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('publicFront/js/popper.min.js') }}"></script>
<script src="{{ asset('publicFront/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('publicFront/js/mediaelement-and-player.min.js') }}"></script>
<script src="{{ asset('publicFront/js/jquery.stellar.min.js') }}"></script>
<script src="{{ asset('publicFront/js/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('publicFront/js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('publicFront/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('publicFront/js/aos.js') }}"></script>
<script src="{{ asset('publicFront/js/main.js') }}"></script>

<!-- Sweet alert -->
<script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function () {

        $('#divCasaVistaGrid').css('display','');
        $('#divCasaVistaList').css('display','none');

        // Obtener Datos Principales
        fnObtenerInfoPrincipal();
        // Obtener Datos Footer
        fnObtenerInfoPiePagina();
        // Obtener Datos Nosotros
        fnObtenerInfoNosotros();
        if (window.location.pathname == "/Contacto") {
            // Obtener Datos de Contacto
            fnObtenerInfoContacto();
        }

        if (window.location.pathname == "/" || window.location.pathname == "/Inicio") {
            fnObtenerPropiedadesCaruosel();
            fnObtenerPropiedades();
            fnObtenerInfoCaruoselConoceSMA();
            $('#idMenuInicioFront').addClass('active');
        }
        
        if (window.location.pathname == "/Asesores") {
            $('#mdlAsesores').addClass('active');
            fnObtenerAsesores();
        }

        if (window.location.pathname == "/Nosotros") {
            $('#idDivNosotrosClase').addClass('mt-6');
        }

        $("#btnEnviarMensaje").click(function(){
            // botón formulario de contacto
            fnContactoEnviarMsj();
        });

        $(".icon-view_module").click(function(){
            $('#divCasaVistaGrid').css('display','');
            $('#divCasaVistaList').css('display','none');
        });

        $(".icon-view_list").click(function(){
            $('#divCasaVistaGrid').css('display','none');
            $('#divCasaVistaList').css('display','');
        });

        $(".FiltrarPropiedades").click(function(){
            // window.location="/Propiedades";
        });

        // Generar visita
        fnRegistrarVisita();
        var dblGlobalTipoCambioUSD = 0;
        
        fnObtenerTipoCambio();


        $(".linkFiltrarPropiedades").click(function(){
            fnCargarPropiedades($("#cmbTipoInmueble").val(), $("#cmbTipoOferta").val(), $("#cmbOdernar").val());
        });

        $(".linkFiltrarPropiedaresReiniciar").click(function(){
            window.location=window.location.origin+"/Propiedades";
        });

        $(".linkFiltrarPropiedaresRenta").click(function(){
            fnCargarPropiedades($("#cmbTipoInmueble").val(), 2, $("#cmbOdernar").val());
        });

        $(".linkFiltrarPropiedaresVenta").click(function(){
            fnCargarPropiedades($("#cmbTipoInmueble").val(), 1, $("#cmbOdernar").val());
        });
        
        $("#cmbOdernar").change(function(){
            fnCargarPropiedades($("#cmbTipoInmueble").val(), $("#cmbTipoOferta").val(), $("#cmbOdernar").val());
        });

        
    });

    $(document).on("click",".lblTipoCambio",function(event) {
        if($(this).data('moneda') =="USD"){
            $(this).attr('aria-label','$ '+number_format( Math.round(parseFloat( $(this).data('nu_precio')) * dblGlobalTipoCambioUSD) ,2,'.',',')+' MXN');      
        }else if($(this).data('moneda') =="MXN" ){
            $(this).attr('aria-label','$ '+number_format(Math.round(parseFloat( $(this).data('nu_precio')) / dblGlobalTipoCambioUSD) ,2,'.',',')+' USD');      
        }
    });

    function fnTooltipTipoCambio(dblGlobalTipoCambioUSD=0){
        $(".divPropiedades").find(".lblTipoCambio").each(function(index){

            if($(this).data('moneda') =="USD"){
                $(this).attr('aria-label','$ '+number_format( Math.round(parseFloat( $(this).data('nu_precio')) * dblGlobalTipoCambioUSD) ,2,'.',',')+' MXN');      
            }else if($(this).data('moneda') =="MXN" ){
                $(this).attr('aria-label','$ '+number_format(Math.round(parseFloat( $(this).data('nu_precio')) / dblGlobalTipoCambioUSD) ,2,'.',',')+' USD');      
            }
        });
    }

    function fnCargarPropiedades(tipoinmueble , tipooferta, odernar) {
        if(tipoinmueble==""){tipoinmueble='-1'}
        if(tipooferta==""){tipooferta='-1'}
        if(odernar==""){odernar='-1'}
        window.location=window.location.origin+"/Propiedades/"+tipoinmueble+"/"+tipooferta+"/"+odernar;
    }

    /**
     * Función para registrar visita
     * @return {[type]} [description]
     */
    function fnRegistrarVisita() {
        $.get(window.location.origin+"/registrarVisita", function(infoData){
            //
        });
    }

    /**
     * Función para obtener los datos configurados de principal
     * @return {[type]} [description]
     */
    function fnObtenerInfoPrincipal() {
        $.get(window.location.origin+"/PrincipalXml", function(xml){
            xml = $( xml );

            var tel1 = xml.find("Telefono").find("Principal").text();
            var tel2 = xml.find("Telefono").find("Secundario").text();
            var cor1 = xml.find("Email").find("Principal").text();
            var cor2 = xml.find("Email").find("Secundario").text();
            var facebook = xml.find("RedesSociales").find("Facebook").text();
            var twitter = xml.find("RedesSociales").find("Twitter").text();
            var linkedin = xml.find("RedesSociales").find("Linkedin").text();
            var whatsApp = xml.find("RedesSociales").find("WhatsApp").text();
            var whatsAppCode = xml.find("RedesSociales").find("WhatsAppCodigo").text();

            // Es un div
            $("#idTelefonoHeader").empty();
            $("#idTelefonoFooter").empty();

            $("#idEmailHeader").empty();
            $("#idEmailFooter").empty();

            // Telefono principal
            $("#idTelefonoHeader").append(""+tel1);
            $("#idTelefonoHeaderAncla").attr("href", "tel:"+tel1);
            $("#idTelefonoFooter").append('<strong>Teléfono: </strong> '+tel1);
            if (document.querySelector("#idContactoTelefono")) {
                // Contacto
                $("#idContactoTelefono").append(""+tel1);
                $("#idContactoTelefono").attr("href", "tel:"+tel1);
            }
            // Telefono secundario
            if (tel2.trim() != "" && tel2.trim() != "undefined" && tel2.trim() != null) {
              $("#idTelefonoFooter").append('<br>'+tel2);
            }
            
            // Correo principal
            $("#idEmailHeader").append(""+cor1);
            $("#idEmailHeaderAncla").attr("href", "mailto:"+cor1);
            $("#idEmailFooter").append('<strong>Correo: </strong> '+cor1);
            if (document.querySelector("#idContactoEmail")) {
                // Contacto
                $("#idContactoEmail").append(""+cor1);
                $("#idContactoEmail").attr("href", "mailto:"+cor1);
            }
            // Correo secundario
            if (cor2.trim() != "" && cor2.trim() != "undefined" && cor2.trim() != null) {
              $("#idEmailFooter").append('<br>'+cor2);
            }
            
            // Anclas header
            $("#idFacebookHeader").attr("href", facebook);
            $("#idTwitterHeader").attr("href", twitter);
            $("#idLinkedinHeader").attr("href", linkedin);
            $("#idWhatsappHeader").attr("href", "https://api.whatsapp.com/send?phone="+whatsAppCode+whatsApp);
            // Anclas footer
            $("#idFacebookFooter").attr("href", facebook);
            $("#idTwitterFooter").attr("href", twitter);
            $("#idLinkedinFooter").attr("href", linkedin);
            $("#idWhatsappFooter").attr("href", "https://api.whatsapp.com/send?phone="+whatsAppCode+whatsApp);

            // validar display
            if (facebook.trim() == "" || facebook.trim() == "undefined" || facebook.trim() == null) {
              $("#idFacebookHeader").addClass("hide");
              $("#idFacebookFooter").addClass("hide");
            }
            if (twitter.trim() == "" || twitter.trim() == "undefined" || twitter.trim() == null) {
              $("#idTwitterHeader").addClass("hide");
              $("#idTwitterFooter").addClass("hide");
            }
            if (linkedin.trim() == "" || linkedin.trim() == "undefined" || linkedin.trim() == null) {
              $("#idLinkedinHeader").addClass("hide");
              $("#idLinkedinFooter").addClass("hide");
            }
            if (whatsApp.trim() == "" || whatsApp.trim() == "undefined" || whatsApp.trim() == null) {
              $("#idWhatsappHeader").addClass("hide");
              $("#idWhatsappFooter").addClass("hide");
            }
        });
    }

    /**
     * Función para obtener los datos configurados de nosotros
     * @return {[type]}          [description]
     */
    function fnObtenerInfoNosotros() {
        $.get(window.location.origin+"/NosotrosXml", function(xml){
            xml = $( xml );

            var principal = xml.find("Titulos").find("Principal").text();
            var secundario = xml.find("Titulos").find("Secundario").text();
            var descripcion = xml.find("Titulos").find("Descripcion").text();
            var imagen = xml.find("Titulos").find("Imagen").text();
            var vision = xml.find("Empresa").find("Vision").text();
            var mision = xml.find("Empresa").find("Mision").text();

            $("#idNosotrosPrincipal").empty();
            $("#idNosotrosSecundario").empty();
            $("#idNosotrosDescripcion").empty();
            $("#idImgNosotrosPrincipal").empty();
            if (document.querySelector("#idNosotrosVision")) {
                $("#idNosotrosVision").empty();
            }
            if (document.querySelector("#idNosotrosMision")) {
                $("#idNosotrosMision").empty();
            }

            $("#idNosotrosPrincipal").append(""+principal);
            $("#idNosotrosSecundario").append(""+secundario);
            $("#idNosotrosDescripcion").append(""+descripcion);
            $("#idImgNosotrosPrincipal").append('<img src="'+imagen+'" alt="Image" class="img-fluid">');
            if (document.querySelector("#idNosotrosVision")) {
                $("#idNosotrosVision").append(""+vision);
            }
            if (document.querySelector("#idNosotrosMision")) {
                $("#idNosotrosMision").append(""+mision);
            }
        });
    }

    /**
     * Función para obtener los datos configurados de contacto
     * @return {[type]}        [description]
     */
    function fnObtenerInfoContacto() {
        $.get(window.location.origin+"/ContactoXml", function(xml){
            xml = $( xml );

            var principal = xml.find("Titulos").find("Principal").text();
            var descripcion = xml.find("Titulos").find("Descripcion").text();
            var maps = xml.find("Links").find("Maps").text();

            $("#idContactoPrincipal").empty();
            $("#idContactoDescripcion").empty();
            $("#idContactoMaps").empty();

            $("#idContactoPrincipal").append(""+principal);
            $("#idContactoDescripcion").append(""+descripcion);
            $("#idContactoMaps").append(""+maps);

            $("iframe").addClass("mapacontacto");
        });
    }

    /**
     * Función para obtener los datos configurados del pie de página
     * @return {[type]}        [description]
     */
    function fnObtenerInfoPiePagina() {
        $.get(window.location.origin+"/PiePaginaXml", function(xml){
            xml = $( xml );

            var principal = xml.find("Titulos").find("Principal").text();
            var descripcion = xml.find("Titulos").find("Descripcion").text();
            var derechos = xml.find("Derechos").find("Texto").text();

            $("#idTituloFooter").empty();
            $("#idDescripcionFooter").empty();
            $("#idDerechosFooter").empty();

            $("#idTituloFooter").append(""+principal);
            $("#idDescripcionFooter").append(""+descripcion);
            $("#idDerechosFooter").append(""+derechos);
        });
    }

    function fnObtenerTipoCambio(){
        var idSeries="SF43718"; //,SF46410 Euro (Separacion por comas)
        var token="9f353f81ff36eda4ff11654cdb1f90ff9c232f2c67156beb3664d6e997ac326b";
        

        $.get("https://www.banxico.org.mx/SieAPIRest/service/v1/series/"+idSeries+"/datos/oportuno?token="+token, function(infoData){
            // console.log(infoData.bmx.series);
            
            if(infoData.bmx.series !== null){
                // console.log(infoData.bmx.series);
                $.each(infoData.bmx.series,function(index, el) {
                    $.each(el.datos,function(index, ella) {
                        dblGlobalTipoCambioUSD = parseFloat(ella.dato);
                        fnTooltipTipoCambio(dblGlobalTipoCambioUSD);
                    });
                });
            }
            // console.log('tipo: ' + dblGlobalTipoCambioUSD);
        });
    }

    /**
     * Función para obtener mostrar el carousel de conoce sma
     * @return {[type]}        [description]
     */
    function fnObtenerInfoCaruoselConoceSMA() {

        var strHtmlRow="";
        var strHtml="";
        var strHtmlRowResponsive="";
        var strHtmlResponsive="";
        var contador =0;
        var posicion =0;
        var posicion2 =0;

        $.get(window.location.origin+"/ServicioCarruselSMA", function(infoData){
            if(infoData.intState){
                if(infoData.publicidad !== null){
                    $.each(infoData.publicidad,function(index, el) {
                        if(contador == 3){
                            strHtmlRow ='<div class="row">'+strHtml+'</div>';
                            $('#ConoceSMANormal').trigger('add.owl.carousel', [$(strHtmlRow), posicion]).trigger('refresh.owl.carousel');
                            strHtml="";
                            contador =0;
                            posicion ++;
                        }

                        strHtml+='<div class="col-md-6 col-lg-4 mb-4" data-aos="fade-up" data-aos-delay="100">';
                        strHtml+='  <a><img src="'+el.ln_url_imagen+'" alt="Image" class="img-fluid-blog"></a>';
                        strHtml+='  <div class="p-4 bg-white">';
                        strHtml+='      <h2 class="h5 text-black mb-3"><a>'+el.ln_titulo+'</a></h2>';
                        strHtml+='      <p>'+el.ln_descripcion+'</p>';
                        strHtml+='  </div>';
                        strHtml+='</div>';

                        strHtmlRowResponsive='<div class="row">';
                        strHtmlRowResponsive+='  <div class="col-md-12" data-aos="fade-up" data-aos-delay="100">';
                        strHtmlRowResponsive+='      <a><img src="'+el.ln_url_imagen+'" alt="Image" class="img-fluid-blog"></a>';
                        strHtmlRowResponsive+='      <div class="p-4 bg-white">';
                        strHtmlRowResponsive+='          <h2 class="h5 text-black mb-3"><a>'+el.ln_titulo+'</a></h2>';
                        strHtmlRowResponsive+='          <p>'+el.ln_descripcion+'</p>';
                        strHtmlRowResponsive+='      </div>';
                        strHtmlRowResponsive+='  </div>';
                        strHtmlRowResponsive+='</div>';

                        $('#ConoceSMAResponsive').trigger('add.owl.carousel', [$(strHtmlRowResponsive), posicion2]).trigger('refresh.owl.carousel');
                        
                        posicion2++;
                        contador++;
                    });

                    if(strHtml!=""){
                        strHtmlRow ='<div class="row">'+strHtml+'</div>';
                        $('#ConoceSMANormal').trigger('add.owl.carousel', [$(strHtmlRow), posicion]).trigger('refresh.owl.carousel');
                        strHtml="";
                            contador =0;
                            posicion ++;
                    }
                }
            }
        });


    }

    /**
     * Función para obtener mostrar el carousel principal
     * @return {[type]}        [description]rrr
     */
    function fnObtenerPropiedadesCaruosel() {
        $.get(window.location.origin+"/ServicioPropiedadesCaruosel", function(infoData){
            if(infoData.intState){
                if(infoData.propiedades !== null){
                    var strHtmlRow="";
                    var btncolor="bg-danger";
                    var spanMoneda="";
                    var txtTipoCambio="";

                    $.each(infoData.propiedades,function(index, el) {

                        spanMoneda="";
                        txtTipoCambio="";

                        if(el.ln_moneda =="USD"){
                            txtTipoCambio="$ "+number_format(Math.round(parseFloat(el.nu_precio) * dblGlobalTipoCambioUSD) ,2,'.',',')+" MXN";
                            spanMoneda=" <strong class='hint--top' aria-label='"+txtTipoCambio+"'><span class='icon-autorenew'></span></strong>";
                        }else if( el.ln_moneda =="MXN"){
                            txtTipoCambio="$ "+number_format(Math.round(parseFloat(el.nu_precio) / dblGlobalTipoCambioUSD) ,2,'.',',')+" USD";
                            spanMoneda=" <strong class='hint--top' aria-label='"+txtTipoCambio+"'><span class='icon-autorenew'></span></strong>";
                        }

                        strHtmlRow ='<div class="site-blocks-cover overlay" style="background-image: url('+el.ln_url_imagen+');" data-aos="fade" data-stellar-background-ratio="5.5">';
                        strHtmlRow +='    <div class="container">';
                        strHtmlRow +='        <div class="row align-items-center justify-content-center text-center">';
                        strHtmlRow +='            <div class="col-md-10">';
                        if(el.nu_tipo_operacion=='1'){
                            btncolor="bg-success";
                        }
                        if(el.nu_tipo_operacion=='2'){
                            btncolor="bg-info";
                        }
                        strHtmlRow +='            <span class="d-inline-block '+btncolor+' text-white px-3 mb-3 property-offer-type rounded">'+el.ln_tipo_operacion+'</span>';
                        strHtmlRow +='            <h1 class="mb-2">'+el.ln_numero_casa+' '+el.ln_nombre+'</h1>';
                        strHtmlRow +='            <p class="mb-5"><strong class="h2 text-success font-weight-bold">$ '+number_format(el.nu_precio,'2','.',',')+' '+el.ln_moneda+' '+spanMoneda+'</strong></p>';
                        strHtmlRow +='            <p><a href="/DetallesPropiedad/'+el.nu_propiedad+'" class="btn btn-white btn-outline-white py-3 px-5 rounded-0 btn-2">Más Información</a></p>';
                        strHtmlRow +='            </div>';
                        strHtmlRow +='        </div>';
                        strHtmlRow +='    </div>';
                        strHtmlRow +='</div>  ';

                        $('#CarouselPrincipal').trigger('add.owl.carousel', [$(strHtmlRow), index]).trigger('refresh.owl.carousel');
                    });
                }
            }
        }); 
    }

    function fnObtenerAsesores(){
        $.get(window.location.origin+"/ServicioAsesores", function(infoData){
            var strHtmlRow="";
            var strHtml="";

            if(infoData.intState){
                if(infoData.asesores !== null){
                    $.each(infoData.asesores,function(index, el) {
                        strHtml += '<div class="col-md-6 col-lg-4 mb-5 mb-lg-5" data-aos="fade-up" data-aos-delay="100">';
                        strHtml += '    <div class="team-member">';
                        strHtml += '        <img src="'+el.ln_url_imagen+'" alt="Image" class="img-fluid rounded mb-4">';
                        strHtml += '        <div class="text">';
                        strHtml += '        <h2 class="mb-1 font-weight-light text-black h4">'+el.ln_nombre+'</h2>';
                        strHtml += '        <span class="d-block mb-2 text-white-opacity-05">'+el.ln_puesto+'</span>';
                        strHtml += '        <p>'+el.ln_descripcion+'</p>';
                        strHtml += '        <p style="font-size:20px;">';

                        if(el.ln_link_facebook != null && el.ln_link_facebook != ""){
                            strHtml += '            <a href="'+el.ln_link_facebook+'" target="_blank" class="text-gray p-2"><span class="icon-facebook"></span></a>';
                        }

                        if(el.ln_link_twitter != null && el.ln_link_twitter != ""){
                            strHtml += '            <a href="'+el.ln_link_twitter +'" target="_blank" class="text-gray p-2"><span class="icon-twitter"></span></a>';
                        }

                        // Es el de instagan antes linkedin
                        if(el.ln_link_linked != null && el.ln_link_linked != ""){
                            strHtml += '            <a href="'+el.ln_link_linked+'" target="_blank" class="text-gray p-2"><span class="icon-instagram"></span></a>';
                        }

                        if(el.ln_link_whattsapp != null && el.ln_link_whattsapp != ""){
                            strHtml += '            <a href="https://api.whatsapp.com/send?phone='+el.nu_cod_pais+el.ln_link_whattsapp+'" target="_blank" class="text-gray p-2"><span class="icon-whatsapp"></span></a>';
                        }

                        strHtml += '        </p>';

                        if(el.ln_telefono != null &&  el.ln_telefono != ""){
                            strHtml += '        <p><a href="tel:'+el.nu_cod_pais+el.ln_telefono+'" class="btn btn-white btn-outline-primary rounded-0 btn-block">Llamar</a></p>';
                        }

                        if(el.ln_correo != null &&  el.ln_correo != ""){
                            strHtml += '        <p><a href="#" class="btn btn-white btn-outline-primary rounded-0 btn-block" data-toggle="modal" data-correo="'+el.ln_correo+'" data-nombre="'+el.ln_nombre+'" data-target="#mdlEnviarCorreo">Enviar Correo</a></p>';
                        }

                        strHtml += '        </div>';
                        strHtml += '    </div>';
                        strHtml += '</div>';
                    });

                    $('#divAsesores').empty();
                    $('#divAsesores').append(strHtml);
                }
            }
        });
    }

    function fnObtenerPropiedades(){
        $.get(window.location.origin+"/ServicioPropiedades", function(infoData){
            // console.log(infoData);
            var strHtmlGrid="";
            var strHtmlList="";
            var btncolor="bg-danger";
            var spanMoneda="";
            var txtTipoCambio="";
            var cont =0;

            if(infoData.intState){
                if(infoData.propiedades !== null){
                    $.each(infoData.propiedades,function(index, el) {
                        if(cont==3){
                            return false;
                        }
                        if(el.nu_tipo_operacion=='1'){
                            btncolor="bg-success";
                        }
                        if(el.nu_tipo_operacion=='2'){
                            btncolor="bg-info";
                        }

                        spanMoneda="";
                        txtTipoCambio="";

                        if(el.ln_moneda =="USD"){
                            txtTipoCambio="$ "+number_format( Math.round(parseFloat(el.nu_precio) * dblGlobalTipoCambioUSD) ,2,'.',',')+" MXN";
                            spanMoneda=" <div class='hint--top' aria-label='"+txtTipoCambio+"'><span class='icon-autorenew'></span></div>";
                        }else if(el.ln_moneda =="MXN"){
                            txtTipoCambio="$ "+number_format( Math.round(parseFloat(el.nu_precio) / dblGlobalTipoCambioUSD) ,2,'.',',')+" USD";
                            spanMoneda=" <div class='hint--top' aria-label='"+txtTipoCambio+"'><span class='icon-autorenew'></span></div>";
                        }

                        strHtmlGrid+='<div class="col-md-6 col-lg-4 mb-4">';
                        strHtmlGrid+='    <div class="property-entry h-100">';
                        strHtmlGrid+='    <a href="/DetallesPropiedad/'+el.nu_propiedad+'" class="property-thumbnail">';
                        strHtmlGrid+='        <div class="offer-type-wrap">';
                        strHtmlGrid+='        <span class="offer-type '+btncolor+'">'+el.ln_tipo_operacion+'</span>';
                        strHtmlGrid+='        </div>';
                        strHtmlGrid+='        <img src="'+el.ln_url_imagen+'" alt="Image" class="img-fluid">';
                        strHtmlGrid+='    </a>';
                        strHtmlGrid+='    <div class="p-4 property-body">';
                        strHtmlGrid+='        <h2 class="property-title"><a href="/DetallesPropiedad/'+el.nu_propiedad+'">'+el.ln_nombre+'</a></h2>';
                        strHtmlGrid+='        <span class="property-location d-block mb-3"><a href="'+el.ln_url_mapa+'" target="_blank"><span class="property-icon icon-room"></span> '+el.ln_numero_casa+' '+el.ln_direccion+'</a></span>';
                        strHtmlGrid+='        <strong class="property-price text-primary mb-3 d-block text-success">$ '+number_format(el.nu_precio,'2','.',',')+' '+el.ln_moneda+' '+ spanMoneda+'</strong>';
                        strHtmlGrid+='        <ul class="property-specs-wrap mb-3 mb-lg-0">';
                        strHtmlGrid+='        <li>';
                        strHtmlGrid+='            <span class="property-specs">Habitaciones</span>';
                        strHtmlGrid+='            <span class="property-specs-number">'+el.nu_dormitorio+'</span>';
                        strHtmlGrid+='        </li>';
                        strHtmlGrid+='        <li>';
                        strHtmlGrid+='            <span class="property-specs">Baños</span>';
                        strHtmlGrid+='            <span class="property-specs-number">'+el.nu_banio+'</span>';
                        strHtmlGrid+='        </li>';
                        strHtmlGrid+='        <li>';
                        strHtmlGrid+='            <span class="property-specs">Metros<sup>2</sup></span>';
                        strHtmlGrid+='            <span class="property-specs-number">'+number_format(el.nu_metros_terreno,'0','.',',')+'</span>';
                        strHtmlGrid+='        </li>';
                        strHtmlGrid+='        </ul>';
                        strHtmlGrid+='    </div>';
                        strHtmlGrid+='    </div>';
                        strHtmlGrid+='</div>';

                        strHtmlList+='<div class="row mb-4"></div><div class="col-md-12">';
                        strHtmlList+='    <div class="property-entry horizontal d-lg-flex">';
                        strHtmlList+='        <a href="/DetallesPropiedad/'+el.nu_propiedad+'" class="property-thumbnail h-100">';
                        strHtmlList+='            <div class="offer-type-wrap">';
                        strHtmlList+='            <span class="offer-type '+btncolor+'">'+el.ln_tipo_operacion+'</span>';
                        strHtmlList+='            </div>';
                        strHtmlList+='            <img src="'+el.ln_url_imagen+'" alt="Image" class="img-fluid">';
                        strHtmlList+='        </a>';
                        strHtmlList+='        <div class="p-4 property-body">';
                        strHtmlList+='            <h2 class="property-title"><a href="/DetallesPropiedad/'+el.nu_propiedad+'">'+el.ln_nombre+'</a></h2>';
                        strHtmlList+='            <span class="property-location d-block mb-3"><span class="property-icon icon-room"></span> '+el.ln_numero_casa+' '+el.ln_direccion+'</span>';
                        strHtmlList+='            <strong class="property-price text-primary mb-3 d-block text-success">$ '+number_format(el.nu_precio,'2','.',',')+' '+el.ln_moneda+ ' '+ spanMoneda+'</strong>';
                        strHtmlList+='            <p>'+el.ln_detalles+'</p>';
                        strHtmlList+='            <ul class="property-specs-wrap mb-3 mb-lg-0">';
                        strHtmlList+='            <li>';
                        strHtmlList+='                <span class="property-specs">Habitaciones</span>';
                        strHtmlList+='                <span class="property-specs-number">'+el.nu_dormitorio+'</span> ';
                        strHtmlList+='            </li>';
                        strHtmlList+='            <li>';
                        strHtmlList+='                <span class="property-specs">Baño</span>';
                        strHtmlList+='                <span class="property-specs-number">'+el.nu_banio+'</span>';
                        strHtmlList+='            </li>';
                        strHtmlList+='            <li>';
                        strHtmlList+='                <span class="property-specs">Metros<sup>2</sup></span>';
                        strHtmlList+='                <span class="property-specs-number">'+number_format(el.nu_metros_terreno,'0','.',',')+'</span>';
                        strHtmlList+='            </li>';
                        strHtmlList+='            </ul>';
                        strHtmlList+='        </div>';
                        strHtmlList+='    </div>';
                        strHtmlList+='</div></div>';

                        cont ++;

                    });
                    $("#divCasaVistaGrid").empty();
                    $("#divCasaVistaGrid").append(strHtmlGrid);

                    $("#divCasaVistaList").empty();
                    $("#divCasaVistaList").append(strHtmlList);
                }
            }
        });
    }



    function fnAjaxSelect2(tipo, ruta, params){
        var url = window.location.origin+"/"+ruta;
        var infoData = [];

        $.ajax({
            async:false,
            url:  url,
            type: tipo,
            dataType: "json",
            data: params,
            cache: false,
            contentType: false,
            processData: false
        })
        .done(function(data){
            if(data.intState){
                infoData =  data;
            }else{
                swal({
                    title: "Conexión",
                    text: data.strMensaje,
                    type: "error"
                });
                infoData =  data;
            }
        });

        return infoData;
    }

    /**
     * Función para enviar el mensaje de contacto
     * @return {[type]} [description]
     */
    function fnContactoEnviarMsj() {

        $("#txtMessageError").addClass("hide");
        var nombreCampo = {
            'txtName':'',
            'txtTelefono':'',
            'txtEmail':'',
            'txtAsunto':'',
            'txtMessage':''
        };

        var msg = fnValidarFormulario('frmContacto', nombreCampo);

        if(msg){ return; }
        
        if ($("#txtMessage").val() == "" || $("#txtMessage").val() == null || $("#txtMessage").val() == "undefined") {
            $("#txtMessageError").removeClass("hide");
            return;
        }

        var formData = new FormData(document.getElementById("frmContacto"));
        formData.append("_method", 'POST');
        var infoData = fnAjaxSelect2('POST','contactoEnviarMsj',formData);
        // console.log("infoData: "+JSON.stringify(infoData));

        if(infoData.intState){
            $("#txtName").val("");
            $("#txtTelefono").val("");
            $("#txtEmail").val("");
            $("#txtAsunto").val("");
            $("#txtMessage").val("");

            swal({ title: infoData.strMensaje, text: '', type: 'success' });
        }
    }

    /**
     * Función para validar campos obligatorios del formulario
     * @param  {[type]} idFormulario Id del formulario
     * @param  {[type]} nombreCampo  Array con los datos a validar
     * @return {[type]}              [description]
     */
    function fnValidarFormulario(idFormulario, nombreCampo) {
        var msg = false;
        var params = getParams(idFormulario), campos = getMatchets(idFormulario);

        $.each(nombreCampo,function(index, el) {
            // Eliminar clase
            $("#"+index+"Error").addClass("hide");
        });
        
        $.each(campos,function(index, el) {
            if($("#"+el).is(":disabled")){ return; }
            if(nombreCampo[el]===undefined){ return; }
            if(!params.hasOwnProperty(el)){ return; }
            if(params[el]!=0&&params[el]!=-1){ return; }
            msg = true;
            $("#"+el+"Error").removeClass("hide");
        });
        
        // if(msg){ swal({title: "Información!", text: 'Campos vacíos', type: "warning"}); }

        return msg;
    }

    /**
     * Función para agregar los nombres
     * @param  {[type]} id [description]
     * @return {[type]}    [description]
     */
    function getMatchets(id) {
        var params = [];
        $('#'+id).find('input[name], select[name], textarea[name]').each(function(index, el) {
            var $self = $(this);
            params.push($self.attr('name'));
        });
        return params;
    }

    /**
     * Función para agregar los nombres
     * @param  {[type]} id [description]
     * @return {[type]}    [description]
     */
    function getParams(id) {
        var params = {};
        $('#'+id).find('input[name], select[name], textarea[name]').each(function(index, el) {
            var $self = $(this);
            //if($self.val() !== undefined) {
                params[$self.attr('name')] = $self.val();
            //}    
        });
        return params;
    }

    function number_format (number, decimals, dec_point, thousands_sep) {
        // Strip all characters but numerical ones.
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + Math.round(n * k) / k;
            };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }

    function fnListadoSelectGeneral(elemento, dataJson, limpiar = 0) {
        var listado = "";

        if ($(""+elemento).prop("multiple")) {
            $(""+elemento).empty();
        }

        if (limpiar == 1) {
            $(""+elemento).empty();
            if (!$(""+elemento).prop("multiple")) {
                listado += "<option value=''>Seleccionar</option>";
            }
        }

        var valorSelect = "";
        if ($(""+elemento).prop("value")) {
            valorSelect = $(""+elemento).prop("value");
        }

        var valor = "";
        var texto = "";
        var contador = 1;
        var selected = '';

        for (var info in dataJson) {
            contador = 1;

            for (var info2 in dataJson[info]) {
                if (contador == 1) {
                    valor = dataJson[info][info2];
                } else {
                    texto = dataJson[info][info2];
                }
                contador ++;
            }

            selected = '';
            if (valorSelect == valor) {
                selected = 'selected="true"';
            }

            listado += "<option value='"+valor+"' "+selected+">"+texto+"</option>";
        }

        $(""+elemento).append(listado);

        // fnFormatoSelectGeneral(""+elemento);

        // if (limpiar == 1) {
        //     $(''+elemento).multiselect('rebuild');
        // }
    }

    if (document.querySelector(".selectTipoInmueble")) {
        $.get(window.location.origin+"/ServicioComboTipoInmuebles", function(infoData){
            if(infoData.intState){
                if(infoData.datos !== null){
                    fnListadoSelectGeneral('.selectTipoInmueble', infoData.datos, 1);
                }
            }
        });
    }

    if (document.querySelector(".selectTipoOferta")) {
        $.get(window.location.origin+"/ServicioComboTipoOperacion", function(infoData){
            if(infoData.intState){
                if(infoData.datos !== null){
                    fnListadoSelectGeneral('.selectTipoOferta', infoData.datos, 1);
                }
            }
        });
    }

</script>