<!DOCTYPE html>
<html>
<head>
  <title>Mensaje de Contacto</title>
</head>
<body>
  <h3>Mensaje de TUCASASMA</h3>
  <p>Información de Contacto</p>
  <p><strong>* Nombre:</strong> {{ $parametros['txtName'] }}</p>
  <p><strong>* Teléfono:</strong> {{ $parametros['txtTelefono'] }}</p>
  <p><strong>* Correo Eletrónico:</strong> {{ $parametros['txtEmail'] }}</p>
  <p><strong>Asunto:</strong> </p>
  <p>{{ $parametros['txtAsunto'] }}</p>
  <p><strong>Mensaje:</strong> </p>
  <p>{{ $parametros['message'] }}</p>
</body>
</html>