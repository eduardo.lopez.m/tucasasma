<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

session_start();

/**
 * Rutas Página Inicio
 */
Route::get('/', function () {
    return view('frontend.index');
});

Route::get('/Inicio', function () {
    return view('frontend.index');
});

Route::get('/Propiedades/{tipoinmueble?}/{tipooperacion?}/{ordenar?}', 'AdminPropiedadesController@fnPropiedades');

Route::get('/DetallesPropiedad/{nu_propiedad}', 'AdminPropiedadesController@fnDetallePropiedad');

Route::get('/Asesores', function () {
    return view('frontend.asesores');
});

Route::get('/Nosotros', function () {
    return view('frontend.nosotros');
});

Route::get('/Contacto', function () {
    return view('frontend.contacto');
});

Route::get('/PrincipalXml','PrincipalController@verXml');
Route::get('/NosotrosXml','NosotrosController@verXml');
Route::get('/ContactoXml','ContactoController@verXml');
Route::get('/PiePaginaXml','PiePaginaController@verXml');
Route::get('/ServicioAsesores','AdminAsesoresController@fnServicioAsesores');
Route::get('/ServicioPropiedades','AdminPropiedadesController@fnServicioPropiedades');
Route::get('/ServicioPropiedadesCaruosel','AdminPropiedadesController@fnServicioPropiedadesCaruosel');
Route::get('/ServicioCarruselSMA','CarruselSmaController@fnServicioCarruselSMA');
Route::get('/ServicioComboMonedas', "MonedasController@index");
Route::get('/ServicioComboTipoInmuebles', "TipoInmuebleController@index");
Route::get('/ServicioComboTipoOperacion', "TipoOperacionesController@index");

Route::post('contactoEnviarMsj','ContactoController@enviarEmail');
Route::get('registrarVisita','ContactoController@registrarVisita');
Route::get('totalVisitas','ContactoController@totalVisitas');


/**
 * Rutas Página Inicio
 */

Route::get('/admin', function () {
    return view('auth.login');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function() {


    Route::get('/abcUsuarios', function () {
        return view('backend.abcUsuarios');
    });
    Route::resource('Users','UsersController');

    Route::get('/home', function () {
        return view('backend.principal');
    });

    // Configuración Principal
    Route::get('/AdminPrincipal', function () {
        return view('backend.principal');
    });
    Route::post('principalAjax','PrincipalController@index');

    // Configuración Nosotros
    Route::get('/AdminNosotros', function () {
        return view('backend.nosotros');
    });
    Route::post('nosotrosAjax','NosotrosController@index');

    // Configuración Contacto
    Route::get('/AdminContacto', function () {
        return view('backend.contacto');
    });
    Route::post('contactoAjax','ContactoController@index');

    // Mensajes de contacto
    Route::get('/AdminContactoMsg', function () {
        return view('backend.admincontactomsg');
    });
    Route::post('contactosPanel','ContactoController@verContactos');
    Route::post('contactosMsg','ContactoController@verMensajes');

    // Configuración Pie de Pagina
    Route::get('/AdminPiePagina', function () {
        return view('backend.piepagina');
    });
    Route::post('piePaginaAjax','PiePaginaController@index');
    
    /* RUTAS DE ADMIN PROPIEDADES */
    Route::get('/AdminPropiedades', function () {
        return view('backend.adminpropiedades');
    });
    Route::resource('urlAdminPropiedades','AdminPropiedadesController');

    Route::get('/AdminAsesores', function () {
        return view('backend.adminasesores');
    });
    Route::resource('urlAdminAsesores','AdminAsesoresController');

    /* RUTAS DE CARRUSEL */
    Route::get('/AdminCarruselSMA', function () {
        return view('backend.carruselsma');
    });
    Route::resource('CarruselSMA','CarruselSmaController');

    Route::resource('PropiedadesImagenes','PropiedadesImagenesController');

    /*Rutas Combos */
    Route::get('/fnComboMonedas', "MonedasController@index");
    Route::get('/fnComboTipoInmuebles', "TipoInmuebleController@index");
    Route::get('/fnComboTipoOperacion', "TipoOperacionesController@index");
});