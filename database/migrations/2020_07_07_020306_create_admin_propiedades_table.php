<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminPropiedadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_propiedades', function (Blueprint $table) {
            $table->bigIncrements('nu_propiedad');
            $table->string('ln_nombre');
            $table->string('ln_numero_casa')->nullable();
            $table->smallInteger('nu_dormitorio')->nullable();
            $table->string('nu_banio',20)->nullable();
            $table->smallInteger('nu_cochera')->nullable();
            $table->smallInteger('nu_metros_terreno')->nullable();
            $table->smallInteger('nu_metros_vivienda')->nullable();
            $table->smallInteger('nu_tipo_inmueble');
            $table->smallInteger('nu_tipo_operacion');
            $table->float('nu_precio', 10, 2);
            $table->string('ln_moneda',10);
            $table->string('ln_correo')->nullable();
            $table->string('ln_telefono')->nullable();
            $table->text('ln_detalles')->nullable();
            $table->text('ln_direccion')->nullable();
            $table->text('ln_url_mapa')->nullable();
            $table->smallInteger('nu_principal');
            $table->smallInteger('nu_activo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_propiedades');
    }
}
