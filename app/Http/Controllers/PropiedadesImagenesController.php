<?php

namespace App\Http\Controllers;

use App\PropiedadesImagenes;
use Illuminate\Http\Request;

class PropiedadesImagenesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PropiedadesImagenes  $propiedadesImagenes
     * @return \Illuminate\Http\Response
     */
    public function show(PropiedadesImagenes $propiedadesImagenes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PropiedadesImagenes  $propiedadesImagenes
     * @return \Illuminate\Http\Response
     */
    public function edit(PropiedadesImagenes $propiedadesImagenes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PropiedadesImagenes  $propiedadesImagenes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PropiedadesImagenes $propiedadesImagenes, $nu_imagen)
    {
        $propiedad = PropiedadesImagenes::where('nu_imagen', $nu_imagen)->first();

        $imagenes = PropiedadesImagenes::where('nu_propiedad', $propiedad['nu_propiedad'])
                                        ->where('nu_principal', '1')
                                        ->first();

        $imagenes->update(['nu_principal'=>'0']);

        $propiedad->update(['nu_principal'=>'1']);
        return response()->json(["intState"=>1,"strMensaje"=>"Se modificó correctamente","propiedad"=>compact('propiedad')],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PropiedadesImagenes  $propiedadesImagenes
     * @return \Illuminate\Http\Response
     */
    public function destroy(PropiedadesImagenes $propiedadesImagenes, $nu_imagen)
    {
        $imagen = PropiedadesImagenes::findOrFail($nu_imagen);
        $imagen->delete();
        return response()->json(["intState"=>1,"strMensaje"=>"Se eliminó correctamente la imagen","corrida"=>$imagen],200);

    }
}
