<?php

namespace App\Http\Controllers;

use App\AdminPropiedades;
use App\PropiedadesImagenes;
use App\TipoInmueble;
use App\TipoOperaciones;
use Illuminate\Http\Request;

class AdminPropiedadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataRequest = request()->all();
        $propiedades = AdminPropiedades::LnNombre($dataRequest['ln_nombre'])
                                        ->LnMoneda($dataRequest['ln_moneda'])
                                        ->get();
        return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvieron las propiedades","propiedades"=>$propiedades],200) ;
    }

    public function fnServicioPropiedades()
    {
        $propiedades = AdminPropiedades::select(
            'admin_propiedades.nu_propiedad',
            'admin_propiedades.ln_nombre',
            'admin_propiedades.ln_numero_casa',
            'admin_propiedades.nu_precio',
            'admin_propiedades.ln_moneda',
            'admin_propiedades.nu_banio',
            'admin_propiedades.nu_cochera',
            'admin_propiedades.nu_cochera',
            'admin_propiedades.nu_dormitorio',
            'admin_propiedades.nu_metros_vivienda',
            'admin_propiedades.nu_tipo_inmueble',
            'admin_propiedades.nu_tipo_operacion',
            'admin_propiedades.ln_direccion',
            'admin_propiedades.ln_url_mapa',
            'admin_propiedades.ln_detalles',
            'tipo_operaciones.ln_tipo_operacion'
        )
        ->join('tipo_operaciones','tipo_operaciones.nu_tipo_operacion', 'admin_propiedades.nu_tipo_operacion')
        ->PropiedadesImagen()
        ->where('admin_propiedades.nu_activo','1')
        ->orderBy('admin_propiedades.nu_propiedad', 'ASC')
        ->get();

        return response()->json(["intState"=>1,"propiedades"=>$propiedades],200) ;
    }

    public function fnServicioPropiedadesCaruosel()
    {
        $propiedades = AdminPropiedades::select(
                                            'admin_propiedades.nu_propiedad',
                                            'admin_propiedades.ln_nombre',
                                            'admin_propiedades.ln_numero_casa',
                                            'admin_propiedades.nu_precio',
                                            'admin_propiedades.ln_moneda',
                                            'admin_propiedades.nu_tipo_operacion',
                                            'tipo_operaciones.ln_tipo_operacion'
                                        )
                                        ->join('tipo_operaciones','tipo_operaciones.nu_tipo_operacion', 'admin_propiedades.nu_tipo_operacion')
                                        ->PropiedadesImagen()
                                        ->where('admin_propiedades.nu_principal','1')
                                        ->where('admin_propiedades.nu_activo','1')
                                        ->orderBy('admin_propiedades.nu_propiedad', 'ASC')
                                        ->take(4)
                                        ->get();
        return response()->json(["intState"=>1,"propiedades"=>$propiedades],200) ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dataRequest = request()->all();

        $directorio = 'images/propiedades/';
        $arrExtenciones["image/png"] = ".png";
        $arrExtenciones["image/jpg"] = ".jpg";
        $arrExtenciones["image/jpeg"] = ".jpeg";
        
        $estatus=0;
        if(isset($dataRequest['nu_activo'])){
            $estatus=1;
        }

        $principal=0;
        if(isset($dataRequest['nu_principal'])){
            $principal=1;
        }

        // $ln_url_imagen = $directorio ."sinImagen.png";
        // if(isset($dataRequest['ln_url_imagen'])){
        //     $ln_url_imagen=$directorio.date('Ymd_His').$arrExtenciones[$_FILES['ln_url_imagen']['type']];
        //     if(!move_uploaded_file($_FILES['ln_url_imagen']['tmp_name'], $ln_url_imagen)){
        //         $ln_url_imagen = $directorio ."sinImagen.png";
        //     }
        // }
        
        $propiedades=AdminPropiedades::create([
            'ln_nombre' => $dataRequest['ln_nombre'],
            'ln_numero_casa' => $dataRequest['ln_numero_casa'],
            'nu_dormitorio' => $dataRequest['nu_dormitorio'],
            'nu_banio' => $dataRequest['nu_banio'],
            'nu_cochera' => $dataRequest['nu_cochera'],
            'nu_metros_terreno' => $dataRequest['nu_metros_terreno'],
            'nu_metros_vivienda' => $dataRequest['nu_metros_vivienda'],
            'nu_tipo_inmueble' => $dataRequest['nu_tipo_inmueble'],
            'nu_tipo_operacion' => $dataRequest['nu_tipo_operacion'],
            'nu_precio' => $dataRequest['nu_precio'],
            'ln_moneda' => $dataRequest['ln_moneda'],
            'ln_correo' => $dataRequest['ln_correo'],
            'ln_telefono' => $dataRequest['ln_telefono'],
            'ln_detalles' => $dataRequest['ln_detalles'],
            'ln_direccion' => $dataRequest['ln_direccion'],
            'ln_url_mapa' => $dataRequest['ln_url_mapa'],
            'nu_principal' => $principal,
            "nu_activo" => $estatus
        ]);
        
        $principal=1;
        foreach($_FILES['fileImagenes']['tmp_name'] as $key => $tmp_name)
        {
            $file_name = $key.$_FILES['fileImagenes']['name'][$key];
            $file_size =$_FILES['fileImagenes']['size'][$key];
            $file_tmp =$_FILES['fileImagenes']['tmp_name'][$key];
            $file_type=$_FILES['fileImagenes']['type'][$key];  

            if($key >0){
                $principal=0;
            }

            $ln_url_imagen=$directorio.date('Ymd_His')."_".rand(100, 1000).$arrExtenciones[$file_type];
            
            if(move_uploaded_file($file_tmp, $ln_url_imagen)){
                $propiedadesImagenes=PropiedadesImagenes::create([
                    'nu_propiedad' => $propiedades['nu_propiedad'],
                    'ln_url_imagen' => $ln_url_imagen,
                    'ln_nombre_imagen' => $file_name,
                    'nu_principal' => $principal,
                    'nu_activo' => '1'
                ]);
            }
        }


        return response()->json(["intState"=>1,"strMensaje"=>"Se creó correctamente: ". $dataRequest['ln_nombre'],"propiedadess"=>compact('propiedades')],200) ;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdminPropiedades  $adminPropiedades
     * @return \Illuminate\Http\Response
     */
    public function show(AdminPropiedades $adminPropiedades, $nu_propiedad)
    {
        $propiedad = AdminPropiedades::where('nu_propiedad', $nu_propiedad)->first();

        $imagenes = PropiedadesImagenes::where('nu_propiedad', $nu_propiedad)->get();


        return response()->json(["intState"=>1,"strMensaje"=>"Se obtuvó correctamente","propiedades"=>compact("propiedad"), "imagenes" =>$imagenes],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdminPropiedades  $adminPropiedades
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdminPropiedades $adminPropiedades, $nu_propiedad)
    {
        $dataRequest = request()->all();

        if (isset($dataRequest['nu_activo'])) {
            if(!is_numeric($dataRequest['nu_activo'])){
                $dataRequest['nu_activo'] = '1';
            }
        }

        if (isset($dataRequest['nu_principal'])) {
            if(!is_numeric($dataRequest['nu_principal'])){
                $dataRequest['nu_principal'] = '1';
            }
        }

        $propiedad = AdminPropiedades::where('nu_propiedad', $nu_propiedad)->first();

        $propiedad->update($dataRequest);

        $directorio = 'images/propiedades/';
        $arrExtenciones["image/png"] = ".png";
        $arrExtenciones["image/jpg"] = ".jpg";
        $arrExtenciones["image/jpeg"] = ".jpeg";
        
        if(isset($dataRequest['fileImagenes'])){
            foreach($_FILES['fileImagenes']['tmp_name'] as $key => $tmp_name)
            {
                $file_name = $_FILES['fileImagenes']['name'][$key];
                $file_size =$_FILES['fileImagenes']['size'][$key];
                $file_tmp =$_FILES['fileImagenes']['tmp_name'][$key];
                $file_type=$_FILES['fileImagenes']['type'][$key];  
    
                $ln_url_imagen=$directorio.date('Ymd_His')."_".rand(100, 1000).$arrExtenciones[$file_type];
                
                if(move_uploaded_file($file_tmp, $ln_url_imagen)){
                    $propiedadesImagenes=PropiedadesImagenes::create([
                        'nu_propiedad' => $nu_propiedad,
                        'ln_url_imagen' => $ln_url_imagen,
                        'ln_nombre_imagen' => $file_name,
                        'nu_principal' => '0',
                        'nu_activo' => '1'
                    ]);
                }
            }
        }
        
        return response()->json(["intState"=>1,"strMensaje"=>"Se modificó correctamente a: ".$propiedad->ln_nombre,"propiedad"=>compact('propiedad')],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdminPropiedades  $adminPropiedades
     * @return \Illuminate\Http\Response
     */
    public function destroy(AdminPropiedades $adminPropiedades, $nu_propiedad)
    {
        $propiedad = AdminPropiedades::where('nu_propiedad',$nu_propiedad)->first();
        
        if($propiedad){
            $propiedad->delete();
            PropiedadesImagenes::where('nu_propiedad',$nu_propiedad)->delete();
            return response()->json(["intState"=>1,"strMensaje"=>"Se eliminó correctamente la corrida","propiedad"=>$propiedad],200);
        }

        return response()->json(["intState"=>0,"strMensaje"=>"Problmas al eliminar","propiedad"=>$propiedad],200);

    }

    public function fnPropiedades(AdminPropiedades $adminPropiedades, $tipoinmueble ='', $tipooperacion='', $ordenar=''){
        $propiedades = AdminPropiedades::select(
                                            'admin_propiedades.nu_propiedad',
                                            'admin_propiedades.ln_nombre',
                                            'admin_propiedades.ln_numero_casa',
                                            'admin_propiedades.nu_precio',
                                            'admin_propiedades.ln_moneda',
                                            'admin_propiedades.nu_banio',
                                            'admin_propiedades.nu_cochera',
                                            'admin_propiedades.nu_cochera',
                                            'admin_propiedades.nu_dormitorio',
                                            'admin_propiedades.nu_metros_vivienda',
                                            'admin_propiedades.nu_tipo_inmueble',
                                            'admin_propiedades.nu_tipo_operacion',
                                            'admin_propiedades.ln_direccion',
                                            'admin_propiedades.ln_url_mapa',
                                            'admin_propiedades.ln_detalles',
                                            'tipo_operaciones.ln_tipo_operacion'
                                        )
                                        ->join('tipo_operaciones','tipo_operaciones.nu_tipo_operacion', 'admin_propiedades.nu_tipo_operacion')
                                        ->PropiedadesImagen()
                                        ->NuTipoInmueble($tipoinmueble)
                                        ->NuTipoOperacion($tipooperacion)
                                        ->where('admin_propiedades.nu_activo','1')
                                        ->OrdenarPrecio($ordenar)
                                        ->get();
        
        $combotipoinmueble = TipoInmueble::select(['nu_tipo_inmueble','ln_tipo_inmueble'])
                            ->where('nu_activo', '1')
                            ->orderBy('ln_tipo_inmueble', 'ASC')->get();
        
        $combotipooperacion = TipoOperaciones::select(['nu_tipo_operacion','ln_tipo_operacion'])
                ->where('nu_activo', '1')
                ->orderBy('ln_tipo_operacion', 'ASC')->get();

        return view('frontend.propiedades',['propiedades'=>$propiedades, 'tipoinmueble' =>$tipoinmueble, 'combotipoinmueble' =>$combotipoinmueble, 'tipooperacion'=>$tipooperacion, 'combotipooperacion'=>$combotipooperacion, 'ordenar'=>$ordenar]);
    }
    public function fnDetallePropiedad($nu_propiedad){

        $propiedades = AdminPropiedades::select(
                            'admin_propiedades.nu_propiedad',
                            'admin_propiedades.ln_nombre',
                            'admin_propiedades.ln_numero_casa',
                            'admin_propiedades.nu_precio',
                            'admin_propiedades.ln_moneda',
                            'admin_propiedades.nu_banio',
                            'admin_propiedades.nu_cochera',
                            'admin_propiedades.nu_cochera',
                            'admin_propiedades.nu_dormitorio',
                            'admin_propiedades.nu_metros_vivienda',
                            'admin_propiedades.nu_metros_terreno',
                            'admin_propiedades.nu_tipo_inmueble',
                            'admin_propiedades.nu_tipo_operacion',
                            'admin_propiedades.ln_direccion',
                            'admin_propiedades.ln_url_mapa',
                            'admin_propiedades.ln_detalles',
                            'tipo_inmuebles.ln_tipo_inmueble',
                            'tipo_operaciones.ln_tipo_operacion'
                        )
                        ->join('tipo_inmuebles','tipo_inmuebles.nu_tipo_inmueble', 'admin_propiedades.nu_tipo_inmueble')
                        ->join('tipo_operaciones','tipo_operaciones.nu_tipo_operacion', 'admin_propiedades.nu_tipo_operacion')
                        ->PropiedadesImagen()
                        ->where('admin_propiedades.nu_propiedad',$nu_propiedad)
                        ->where('admin_propiedades.nu_activo','1')
                        ->get();
                        
        $imagenes = PropiedadesImagenes::where('nu_propiedad', $nu_propiedad)->get();

        return view('frontend.detallepropiedades',['propiedades'=>$propiedades,'imagenes'=>$imagenes]);
    }
}
