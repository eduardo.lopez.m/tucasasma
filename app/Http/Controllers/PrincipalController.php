<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PrincipalController extends Controller {

    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        if(request()->ajax()){
            $dataRequest = request()->all();

            $xml = new \DomDocument('1.0', 'UTF-8'); //Se crea el docuemnto

            $raiz = $xml->createElement('Principal');
            $raiz = $xml->appendChild($raiz);

            // Telefonos
            $nodo_First = $xml->createElement('Telefono');
            $nodo_First = $raiz->appendChild($nodo_First);

            $nodo_Second = $xml->createElement('Principal', $dataRequest['txtTelPrincipal']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            $nodo_Second = $xml->createElement('Secundario', $dataRequest['txtTelSecundario']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            // Email
            $nodo_First = $xml->createElement('Email');
            $nodo_First = $raiz->appendChild($nodo_First);

            $nodo_Second = $xml->createElement('Principal', $dataRequest['txtEmailPrincipal']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            $nodo_Second = $xml->createElement('Secundario', $dataRequest['txtEmailSecundario']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            // Link
            $nodo_First = $xml->createElement('RedesSociales');
            $nodo_First = $raiz->appendChild($nodo_First);

            $nodo_Second = $xml->createElement('Facebook', $dataRequest['txtLinkFacebook']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            $nodo_Second = $xml->createElement('Twitter', $dataRequest['txtLinkTwitter']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            $nodo_Second = $xml->createElement('Linkedin', $dataRequest['txtLinkLinkedin']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            $nodo_Second = $xml->createElement('WhatsApp', $dataRequest['txtLinkWhats']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            $nodo_Second = $xml->createElement('WhatsAppCodigo', $dataRequest['txtLinkWhatsCod']);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);

            $linkWhats = ""; //
            if (!empty($dataRequest['txtLinkWhats'])) {
                $linkWhats = "https://api.whatsapp.com/send?phone=".$dataRequest['txtLinkWhats'];
            }
            $nodo_Second = $xml->createElement('WhatsAppLink', $linkWhats);
            $nodo_Second = $nodo_First->appendChild($nodo_Second);


            //Se eliminan espacios en blanco
            $xml->preserveWhiteSpace = false;

            //Se ingresa formato de salida
            $xml->formatOutput = true;

            //Se instancia el objeto
            $xml_string =$xml->saveXML();

            //Y se guarda en el nombre del archivo 'achivo.xml', y el obejto nstanciado
            \Storage::disk('local')->put('Principal.xml',$xml_string);

            return response()->json(["intState"=>1,"strMensaje"=>"Información guardada correctamente.","contenido"=>$xml_string],200) ;            
        }else{
            return response()->json(["intState"=>0,"strMensaje"=>"Verifica con el administrador.","contenido"=>""],400) ;
        }
    }

    public function verXml() {
        $xml = \Storage::disk('local')->get('Principal.xml');
        return response($xml)->withHeaders([ 'Content-Type' => 'text/xml']);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */
    public function create()
    {

    }

    /**
    * Store a newly created resource in storage.
    *
    * @return Response
    */
    public function store()
    {

    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function show($id)
    {

    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return Response
    */
    public function edit($id)
    {

    }

    /**
    * Update the specified resource in storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function update($id)
    {

    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return Response
    */
    public function destroy($id)
    {

    }
  
}

?>