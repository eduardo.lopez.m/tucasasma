$(document).ready(function () {

    if (window.location.pathname == "/" || window.location.pathname == "/Inicio") {
        $('#mdlInicio').addClass('active');
    }

    if (window.location.pathname == "/Nosotros") {
        $('#mdlNosotros').addClass('active');
    }

    if (window.location.pathname == "/Contacto") {
        $('#mdlContacto').addClass('active');
    }

});